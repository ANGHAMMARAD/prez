import java.io.*;
import java.lang.reflect.Array;
import java.nio.CharBuffer;
import java.util.ArrayList;

public class TextFile {
    ArrayList<Character> buffer = new ArrayList<>();
    File file;

    TextFile(File file) throws IOException {
        if (file.exists() && file.isFile()) {
            read(file);
        }
    }

    private void read(File file) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        char[] buf = new char[1];
        while (br.read(buf) != -1) {
            for(Character ch : buf) buffer.add(ch);
        }
        br.close();
    }

    TextFile(String name, File folder) throws IllegalArgumentException, IOException {
        if(folder==null || !folder.isDirectory()) throw new IllegalArgumentException();
        file = new File(availableFileName(folder, name));
        if(file.exists()) read(file);
    }

    void save() throws IOException {
        if(!file.exists()) file.createNewFile();
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));

        bw.write(toString().toCharArray());
        bw.flush();
        bw.close();
    }

    void add(String s){
        for(Character ch : s.toCharArray()){
            buffer.add(ch);
        }
    }

    void remove(int amount){
        for(int i=0; i<Math.min(amount, buffer.size()); ++i){
            buffer.remove(buffer.size()-1);
        }
    }

    @Override
    public String toString() {
        char[] res = new char[buffer.size()];
        int i=0;
        for(Character character : buffer){
            res[i++] = character;
        }
        return new String(res);
    }

    private String availableFileName(File folder, String name) {
        return folder.getAbsolutePath()+"/"+name+".txt";
    }

    public int size() {
        return buffer.size();
    }

    public char charAt(int i) {
        return buffer.get(i);
    }
}
