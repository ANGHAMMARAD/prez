public class AddCommand implements Command{
    TextFile textFile;
    String str;

    AddCommand(TextFile textFile, String str){
        this.textFile=textFile;
        this.str=str;
    }


    @Override
    public void execute() {
        textFile.add(str);
    }

    @Override
    public void unexecute() {
        textFile.remove(str.length());
    }
}
