import java.util.LinkedList;

public class TextFileOperationExecutor {
    LinkedList<Command> commands = new LinkedList<>();
    TextFile textFile;

    TextFileOperationExecutor(TextFile textFile){
        this.textFile = textFile;
    }

    void addCommand(Command command){
        commands.add(command);
        command.execute();
    }

    void reverseCommand(){
        if(!commands.isEmpty()) commands.remove(commands.size()-1).unexecute();
    }
}
