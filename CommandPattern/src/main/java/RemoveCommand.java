public class RemoveCommand implements Command{
    TextFile textFile;
    String str;
    int amount;

    RemoveCommand(TextFile textFile, int amount){
        this.textFile=textFile;
        this.amount = Math.min(amount, textFile.size());
    }


    @Override
    public void execute() {
        char[] buffer = new char[amount];
        for(int i=0; i<buffer.length; ++i){
            buffer[i] = textFile.charAt(textFile.size()-amount+i);
        }
        str = new String(buffer);
        textFile.remove(amount);
    }

    @Override
    public void unexecute() {
        textFile.add(str);
    }
}
