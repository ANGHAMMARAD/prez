import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
    TextFile textFile = new TextFile("mainTest", new File("/"));
    textFile.remove(textFile.size());

    TextFileOperationExecutor executor = new TextFileOperationExecutor(textFile);
    System.out.println(textFile);
    executor.addCommand(new AddCommand(textFile, "1"));
    System.out.println(textFile);
    executor.addCommand(new AddCommand(textFile, "2"));
    System.out.println(textFile);
        executor.addCommand(new AddCommand(textFile, "3"));
        System.out.println(textFile);
    executor.addCommand(new RemoveCommand(textFile, 1));
    System.out.println(textFile);
        executor.addCommand(new RemoveCommand(textFile, 1));
        System.out.println(textFile);
        executor.addCommand(new AddCommand(textFile, "2"));
        System.out.println(textFile);

    System.out.println("Reverse all commands:");
    System.out.println(textFile);
    while (!executor.commands.isEmpty()){
        executor.reverseCommand();
        System.out.println(textFile);
    }
    }
}
