import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

public class CommandTest {

    @Test
    public void testCommands() throws IOException {
        TextFile textFile = new TextFile("TestFile", new File("/"));
        TextFileOperationExecutor textFileOperationExecutor = new TextFileOperationExecutor(textFile);

        for(int i=0; i<=9; ++i){
            textFileOperationExecutor.addCommand(new AddCommand(textFile, ""+i+""+i));
            textFileOperationExecutor.addCommand(new RemoveCommand(textFile, 1));
        }

        char[] correctAnswer = new String("0123456789").toCharArray();
        Assert.assertArrayEquals(textFile.toString().toCharArray(), correctAnswer);
    }
}