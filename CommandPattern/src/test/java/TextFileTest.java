import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.*;

public class TextFileTest {

    @org.junit.Test
    public void save() throws IOException {
        TextFile textFile = new TextFile("test1", new File("/"));
        String str = "Tetshhhhhhhhh";
        textFile.add(str);
        textFile.save();

        TextFile file = new TextFile("test1", new File("/"));
        Assert.assertArrayEquals(file.toString().toCharArray(), textFile.toString().toCharArray());
    }

    @org.junit.Test
    public void add() throws IOException {
        TextFile textFile = new TextFile("test1", new File("/"));
        String current = textFile.toString();
        String str = "Tetshhhhhhhhh";
        textFile.add(str);
        textFile.remove(str.length());
        Assert.assertArrayEquals(current.toCharArray(), textFile.toString().toCharArray());
    }
}